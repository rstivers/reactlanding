import React, { Component } from "react";
import "./App.css";
import PageController from "./components/pagecontroller";

class App extends Component {
  state = {};

  render() {
    return <PageController />;
  }
}

export default App;
