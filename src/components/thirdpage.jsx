import React, { Component } from "react";

class ThirdPage extends Component {
  state = {
    stylePath: "page3.css"
  };

  constructor(props) {
    super(props);
    this.state = { stylePath: "page3.css" };
  }
  render() {
    return (
      <React.Fragment>
        <link rel="stylesheet" type="text/css" href={this.state.stylePath} />
        <section class="hero is-fullheight">
          <div class="hero-head">
            <nav class="navbar" role="navigation" aria-label="main navigation">
              <div class="navbar-brand">
                <div class="navbar-item">
                  <img src="media/rocket-logo.png" />
                  <p>COMPANY NAME</p>
                </div>
                <a
                  role="button"
                  class="navbar-burger burger"
                  aria-label="menu"
                  aria-expanded="false"
                  data-target="navbarBasicExample"
                >
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                </a>
              </div>

              <div class="navbar-end">
                <div class="navbar-item">
                  <div class="buttons">
                    <a class="button btn-primary">
                      <strong>Account</strong>
                    </a>
                    <a class="button is-light">Products</a>
                  </div>
                </div>
              </div>
            </nav>
          </div>
          <div class="hero-body header-image">
            <img src="media/rocket-logo.png" class="rotate-cw" />
          </div>
          <div class="container header has-text-centered">
            <h1 class="title is-1">Store Name</h1>
            <h2 class="subtitle is-3">Motto Here</h2>
            <div class="columns">
              <div class="column" />
              <div class="column is-three-fifths">
                <form>
                  <div class="field">
                    <div class="control has-icons-left">
                      <input
                        class="input is-expanded is-large"
                        type="product"
                        placeholder="Type a product here"
                      />
                      <span class="icon is-small is-left">
                        <i class="fas map-marker-question" />
                      </span>
                    </div>
                  </div>
                  <a class="button is-large btn-primary">Search</a>
                </form>
              </div>
              <div class="column" />
            </div>
          </div>
        </section>
        <section class="hero is-fullheight">
          <div class="hero-body has-text-centered">
            <div class="container">
              <h1 class="title is-2">Popular Items</h1>
              <div class="columns">
                <div class="column">
                  <img
                    class="image"
                    src="https://bulma.io/images/placeholders/480x480.png"
                  />
                </div>
                <div class="column">
                  <img
                    class="image"
                    src="https://bulma.io/images/placeholders/480x480.png"
                  />
                </div>
                <div class="column">
                  <img
                    class="image"
                    src="https://bulma.io/images/placeholders/480x480.png"
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </React.Fragment>
    );
  }
}

export default ThirdPage;
