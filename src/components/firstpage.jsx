import React, { Component } from "react";

class FirstPage extends Component {
  state = {
    stylePath: "css/page1.css"
  };

  constructor(props) {
    super(props);
    this.state = { stylePath: "page1.css" };
  }
  render() {
    return (
      <section class="hero is-fullheight">
        <link rel="stylesheet" type="text/css" href={this.state.stylePath} />
        <div class="hero-head">
          <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
              <div class="navbar-item">
                <img src="media/logo.png" />
                <p>COMPANY NAME</p>
              </div>
              <a
                role="button"
                class="navbar-burger burger"
                aria-label="menu"
                aria-expanded="false"
                data-target="navbarBasicExample"
              >
                <span aria-hidden="true" />
                <span aria-hidden="true" />
                <span aria-hidden="true" />
              </a>
            </div>

            <div class="navbar-end">
              <div class="navbar-item">
                <div class="buttons">
                  <a class="button is-primary">
                    <strong>About</strong>
                  </a>
                  <a class="button is-light">Contact</a>
                </div>
              </div>
            </div>
          </nav>
        </div>

        <div class="hero-body has-text-centered">
          <div class="container header">
            <img src="media/logo.png" />
            <h1 class="title">Introducing Product</h1>
            <h2 class="subtitle">A Product That Does Things</h2>
            <p>Register Now For Early Access</p>
            <div class="columns">
              <div class="column" />
              <div class="column is-one-third">
                <form>
                  <div class="field has-addons">
                    <div class="control has-icons-left">
                      <input
                        class="input is-expanded is-large"
                        type="email"
                        placeholder="Email Address"
                      />
                      <span class="icon is-small is-left">
                        <i class="fas fa-envelope" />
                      </span>
                    </div>
                    <div class="control">
                      <a class="button is-large is-primary">Submit</a>
                    </div>
                  </div>
                </form>
              </div>
              <div class="column" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default FirstPage;
