import React, { Component } from "react";

class SecondPage extends Component {
  state = {
    stylePath: "page2.css"
  };

  constructor(props) {
    super(props);
    this.state = { stylePath: "page2.css" };
  }
  render() {
    return (
      <div class="has-navbar-fixed-top">
        <link rel="stylesheet" type="text/css" href={this.state.stylePath} />
        <nav
          class="navbar is-fixed-top"
          role="navigation"
          aria-label="main navigation"
        >
          <div class="navbar-brand">
            <div class="navbar-item">
              <img src="media/mtn-logo.png" />
              <p>COMPANY NAME</p>
            </div>
            <a
              role="button"
              class="navbar-burger burger"
              aria-label="menu"
              aria-expanded="false"
              data-target="navbarBasicExample"
            >
              <span aria-hidden="true" />
              <span aria-hidden="true" />
              <span aria-hidden="true" />
            </a>
          </div>

          <div class="navbar-end">
            <div class="navbar-item">
              <div class="buttons">
                <a class="button is-warning" href="#about">
                  <strong>About</strong>
                </a>
                <a class="button is-light">Contact</a>
              </div>
            </div>
          </div>
        </nav>

        <section class="hero is-medium has-navbar landing">
          <div class="hero-head" />

          <div class="hero-body has-text-centered">
            <div class="container header">
              <h1 class="title is-1">Company Name</h1>
              <h2 class="subtitle is-3">We Do Stuff</h2>
              <a class="button is-large is-warning">See Our Work</a>
            </div>
          </div>
          <div class="arrow-down" />
        </section>

        <section class="hero is-medium about" id="about">
          <div class="hero-body has-text-centered">
            <div class="container">
              <h1 class="title">About</h1>
              <div class="columns">
                <div class="column has-background-grey-light">
                  First column <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                </div>
                <div class="column has-background-grey-lighter">
                  Second column <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                </div>
                <div class="column has-background-grey-light">
                  Third column <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                  Primar lorem ipsum dolor sit amet, consectetur adipiscing elit
                  lorem ipsum dolor. <strong>Pellentesque risus mi</strong>,
                  tempus quis placerat ut, porta nec nulla. Vestibulum rhoncus
                  ac ex sit amet fringilla. Nullam gravida purus diam, et dictum{" "}
                  <a>felis venenatis</a> efficitur. Sit amet, consectetur
                  adipiscing elit
                  <br />
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default SecondPage;
