import React, { Component } from "react";
import FirstPage from "./firstpage";
import SecondPage from "./secondpage";
import ThirdPage from "./thirdpage";
import FourthPage from "./fourthpage";

class PageController extends Component {
  state = {
    pageIndex: 2,
    pages: [<FirstPage />, <SecondPage />, <ThirdPage />, <FourthPage />]
  };
  render() {
    return (
      <div>
        <div
          className="arrow arrow-left"
          onClick={() => {
            this.flipPrev();
          }}
        >
          <i className="fas fa-arrow-left fa-5x" />
        </div>
        <div
          className="arrow arrow-right"
          onClick={() => {
            this.flipNext();
          }}
        >
          <i className="fas fa-arrow-right fa-5x" />
        </div>
        {this.state.pages[this.state.pageIndex]}
      </div>
    );
  }

  flipPrev() {
    this.setState({
      pageIndex:
        this.state.pageIndex === 0
          ? this.state.pages.length - 1
          : this.state.pageIndex - 1
    });
  }

  flipNext() {
    this.setState({
      pageIndex:
        this.state.pageIndex === this.state.pages.length - 1
          ? 0
          : this.state.pageIndex + 1
    });
  }
}

export default PageController;
