import React, { Component } from "react";

class FourthPage extends Component {
  state = {
    stylePath: "page4.css"
  };

  constructor(props) {
    super(props);
    this.state = { stylePath: "page4.css" };
  }
  render() {
    return (
      <div>
        <link rel="stylesheet" type="text/css" href={this.state.stylePath} />
        <section class="hero is-medium landing">
          <div class="hero-head">
            <nav
              class="navbar is-fixed-top"
              role="navigation"
              aria-label="main navigation"
            >
              <div class="navbar-brand">
                <div class="navbar-item">
                  <img src="media/rocket-logo.png" />
                  <p>COMPANY NAME</p>
                </div>
                <a
                  role="button"
                  class="navbar-burger burger"
                  aria-label="menu"
                  aria-expanded="false"
                  data-target="navbarBasicExample"
                >
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                </a>
              </div>

              <div class="navbar-end">
                <div class="navbar-item">
                  <div class="buttons">
                    <a class="button is-light" href="#about">
                      Account
                    </a>
                    <a class="button is-light">Contact</a>
                  </div>
                </div>
              </div>
            </nav>
          </div>
          <div class="hero-body">
            <div class="container header">
              <img src="media/rocket-logo.png" />
              <h1 class="title is-1">App Name</h1>
              <h2 class="subtitle is-3">This is an App</h2>
              <a class="button is-large is-light">Download</a>
            </div>
          </div>
          <div class="arrow-down" />
        </section>

        <section class="hero is-large about" id="about">
          <div class="hero-body has-text-centered about-container">
            <div class="columns landing-container">
              <div class="column is-two-fifths" />
              <div class="column is-three-fifths">
                <img src="media/phone.png" />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default FourthPage;
